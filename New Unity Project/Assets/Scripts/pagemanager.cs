using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pagemanager : MonoBehaviour
{
    ///UI buttons
    public GameObject UIbuttonfixed_food;
    public GameObject UIbuttonfixed_home;


    ///Pages

    public GameObject Home;
    public GameObject foodmenu;
     public GameObject MamakDon;

    ///Buttons
    public GameObject MamakDonbtn_function;


    // Start is called before the first frame update
    void Start()
    {
        MamakDon.SetActive(false);
        MamakDonbtn_function.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void uibtn_foodmenu(){
        ///btn
        UIbuttonfixed_food.SetActive(true);
        UIbuttonfixed_home.SetActive(false);

        ///pages
        Home.SetActive(false);
        foodmenu.SetActive(true);

        //buttons

        MamakDonbtn_function.SetActive(true);
    }

     public void uibtn_home(){
         ///btn
        UIbuttonfixed_food.SetActive(false);
        UIbuttonfixed_home.SetActive(true);

        ///pages
        Home.SetActive(true);
        foodmenu.SetActive(false);
    }








    public void MamakDonbtn(){
        MamakDon.SetActive(true);
        foodmenu.SetActive(false);
        Debug.Log("click");
    }
    public void ARscene(){
        SceneManager.LoadSceneAsync(1);
    }
}
